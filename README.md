Various experiments with sphere-mapped procedural game world generation.

The next step should be to generalise a way of applying 2-dimensional convolutions to the surface of the sphere, and then add arbitrarily many channels in place of the heightmap etc.

This should allow some more productive exploration of ways to approximate tectonic drift, erosion and climate.

(c) Thomas Ash 2014-2016 GPL