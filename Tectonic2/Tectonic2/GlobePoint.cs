﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Tectonic2
{
    #region Structs

    //3 double vector
    struct Point3
    {
        private double _x, _y, _z, _latitude, _longitude, _r;

        public Point3(double x = 0, double y = 0, double z = 0, 
            double latitude = 0, double longitude = 0, double r = 0)
        {
            _x = x;
            _y = y;
            _z = z;
            _latitude = latitude;
            _longitude = longitude;
            _r = r;
        }

        public double X { get { return _x; } set { _x = value; } }
        public double Y { get { return _y; } set { _y = value; } }
        public double Z { get { return _z; } set { _z = value; } }
        public double Latitude { get { return _latitude; } set { _latitude = value; } }
        public double Longitude { get { return _longitude; } set { _longitude = value; } }
        public double R { get { return _r; } set { _r = value; } }

        public double Distance(Point3 other)
        {
            return (this - other).Length;
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(_x * _x + _y * _y + _z * _z);
            }
        }

        #region Polar <=> Cartesian

        public void PolarToCartesian()
        {
            _x = _r * Math.Cos(_latitude) * Math.Sin(_longitude);
            _y = _r * Math.Sin(_latitude);
            _z = -_r * Math.Cos(_latitude) * Math.Cos(_longitude);
        }

        public void CartesianToPolar()
        {
            _r = Math.Sqrt(_x * _x + _y * _y + _z * _z);
            _latitude = Math.Asin(_y / _r);
            _longitude = Math.Atan2(_x, -_z);
        }

        #endregion

        #region Statics

        public static double Dot(Point3 a, Point3 b)
        {
            return (a.X * b.X + a.Y * b.Y + a.Z * b.Z);
        }

        public static Point3 Cross(Point3 a, Point3 b)
        {
            Point3 c = new Point3(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
            return c;
        }

        public static Point3 operator +(Point3 a, Point3 b)
        {
            Point3 c = new Point3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
            return c;
        }

        public static Point3 operator -(Point3 a, Point3 b)
        {
            Point3 c = new Point3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            return c;
        }

        public static Point3 operator *(Point3 a, double b)
        {
            Point3 c = new Point3(a.X * b, a.Y * b, a.Z * b);
            return c;
        }

        #endregion

        #region Rotate

        public Point3 RotateX(double theta)
        {
            double sinTheta = Math.Sin(theta);
            double cosTheta = Math.Cos(theta);
            return new Point3(_x, 
                cosTheta * _y - sinTheta * _z, 
                sinTheta * _y + cosTheta * _z);
        }

        public Point3 RotateY(double theta)
        {
            double sinTheta = Math.Sin(theta);
            double cosTheta = Math.Cos(theta);
            return new Point3(cosTheta * _x + sinTheta * _z,
                _y,
                cosTheta * _z - sinTheta * _x);
        }

        public Point3 RotateZ(double theta)
        {
            double sinTheta = Math.Sin(theta);
            double cosTheta = Math.Cos(theta);
            return new Point3(cosTheta * _x - sinTheta * _y,
                sinTheta * _x + cosTheta * _y,
                _z);
        }

        #endregion

        public override string ToString() { return String.Format("[{0:0.000}, {1:0.000}, {2:0.000}]", X, Y, Z); }

        public string LatLonString()
        {
            return String.Format("[{0:0.000}, {1:0.000}]", _latitude, _longitude);
        }
    }

    //pointers to two GlobePoints. a GlobePoint can return a list of these.
    //equals operators overridden to ignore order
    struct Edge
    {
        private GlobePoint[] _points;
        private uint _hash;

        public GlobePoint A { get { return _points[0]; } }
        public GlobePoint B { get { return _points[1]; } }

        public Edge(GlobePoint a, GlobePoint b)
        {
            if (a.Id == b.Id) throw new Exception("dang it");

            _points = new GlobePoint[2];
            _points[0] = a;
            _points[1] = b;

            _points = _points.OrderBy(x => x.Id).ToArray();

            _hash = 2166136261;
            for (int i = 0; i < 2; i++)
            {
                _hash = unchecked(_hash ^ _points[i].Id);
                _hash = unchecked(_hash * 16777619);
            }
        }

        public static bool operator ==(Edge a, Edge b)
        {
            if (a.A.Id == b.A.Id && a.B.Id == b.B.Id) return true;
            return false;
        }

        public static bool operator !=(Edge a, Edge b)
        {
            if (a.A.Id == b.A.Id && a.B.Id == b.B.Id) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Edge)) return false;
            Edge other = (Edge)obj;
            if (other.A.Id != A.Id) return false;
            if (other.B.Id != B.Id) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return unchecked((int)_hash);
        }

        public double Length
        {
            get 
            {
                return _points[0].Point.Distance(_points[1].Point);
            }
        }

    }


    //pointers to three GlobePoints. a GlobePoint can return a list of these.
    //equals operators overridden to ignore order
    struct Triangle
    {
        private GlobePoint[] _points;
        private uint _hash;

        public GlobePoint A { get { return _points[0]; } }
        public GlobePoint B { get { return _points[1]; } }
        public GlobePoint C { get { return _points[2]; } }

        public Triangle(GlobePoint a, GlobePoint b, GlobePoint c)
        {
            if (a.Id == b.Id || a.Id == c.Id || b.Id == c.Id) throw new Exception("dang it");

            _points = new GlobePoint[3];
            _points[0] = a;
            _points[1] = b;
            _points[2] = c;

            _points = _points.OrderBy(x => x.Id).ToArray();

            _hash = 2166136261;
            for (int i = 0; i < 3; i++)
            {
                _hash = unchecked(_hash ^ _points[i].Id);
                _hash = unchecked(_hash * 16777619);
            }

        }

        public static bool operator ==(Triangle a, Triangle b)
        {
            if (a.A.Id == b.A.Id && a.B.Id == b.B.Id && a.C.Id == b.C.Id) return true;
            return false;
        }

        public static bool operator !=(Triangle a, Triangle b)
        {
            if (a.A.Id == b.A.Id && a.B.Id == b.B.Id && a.C.Id == b.C.Id) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Triangle)) return false;
            Triangle other = (Triangle)obj;
            if (other.A.Id != A.Id) return false;
            if (other.B.Id != B.Id) return false;
            if (other.C.Id != C.Id) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return unchecked((int)_hash);
        }

        public override string ToString() { return String.Format("[{0}, {1}, {2}]", A.Point.ToString(), B.Point.ToString(), C.Point.ToString()); }

        public Point3 Centre
        {
            get
            {
                Point3 centre = new Point3((_points[0].X + _points[1].X + _points[2].X) / 3,
                    (_points[0].Y + _points[1].Y + _points[2].Y) / 3,
                    (_points[0].Z + _points[1].Z + _points[2].Z) / 3);
                centre.CartesianToPolar();
                return centre;
            }
        }

        public double MinLat
        {
            get
            {
                return A.Point.Latitude < B.Point.Latitude ?
                    A.Point.Latitude < C.Point.Latitude ? A.Point.Latitude : C.Point.Latitude :
                    B.Point.Latitude < C.Point.Latitude ? B.Point.Latitude : C.Point.Latitude;
            }
        }

        public double MaxLat
        {
            get
            {
                return A.Point.Latitude > B.Point.Latitude ?
                    A.Point.Latitude > C.Point.Latitude ? A.Point.Latitude : C.Point.Latitude :
                    B.Point.Latitude > C.Point.Latitude ? B.Point.Latitude : C.Point.Latitude;
            }
        }

        public double MinLong
        {
            get
            {
                double minLong = A.Point.Longitude;
                if (B.Point.Longitude < minLong) minLong = B.Point.Longitude;
                if (C.Point.Longitude < minLong) minLong = C.Point.Longitude;

                double maxLong = A.Point.Longitude;
                if (B.Point.Longitude > maxLong) maxLong = B.Point.Longitude;
                if (C.Point.Longitude > maxLong) maxLong = C.Point.Longitude;

                if (maxLong - minLong < Math.PI) return minLong;

                //wraparound. return lowest longitude that is still > pi

                minLong = A.Point.Longitude;
                if (B.Point.Longitude > Math.PI && B.Point.Longitude < minLong) minLong = B.Point.Longitude;
                if (C.Point.Longitude > Math.PI && C.Point.Longitude < minLong) minLong = C.Point.Longitude;

                return minLong;
            }
        }

        public double MaxLong
        {
            get
            {
                double minLong = A.Point.Longitude;
                if (B.Point.Longitude < minLong) minLong = B.Point.Longitude;
                if (C.Point.Longitude < minLong) minLong = C.Point.Longitude;

                double maxLong = A.Point.Longitude;
                if (B.Point.Longitude > maxLong) maxLong = B.Point.Longitude;
                if (C.Point.Longitude > maxLong) maxLong = C.Point.Longitude;

                if (maxLong - minLong < Math.PI) return maxLong;

                //wraparound. return highest longitude that is still < pi

                maxLong = A.Point.Longitude;
                if (B.Point.Longitude < Math.PI && B.Point.Longitude > maxLong) maxLong = B.Point.Longitude;
                if (C.Point.Longitude < Math.PI && C.Point.Longitude > maxLong) maxLong = C.Point.Longitude;

                return maxLong;
            }
        }

        public Tuple<double, double>[] LongBounds
        {
            get
            {
                List<Tuple<double, double>> bounds = new List<Tuple<double, double>>();

                if ((C.Point.Longitude - A.Point.Longitude) > Math.PI && (C.Point.Longitude - B.Point.Longitude) > Math.PI)       //c isolated on RHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, A.Point.Longitude > B.Point.Longitude ? A.Point.Longitude : B.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(C.Point.Longitude, Math.PI));
                }
                else if ((B.Point.Longitude - A.Point.Longitude) > Math.PI && (B.Point.Longitude - C.Point.Longitude) > Math.PI)       //b isolated on RHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, A.Point.Longitude > C.Point.Longitude ? A.Point.Longitude : C.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(B.Point.Longitude, Math.PI));
                }
                else if ((A.Point.Longitude - B.Point.Longitude) > Math.PI && (A.Point.Longitude - C.Point.Longitude) > Math.PI)       //a isolated on RHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, B.Point.Longitude > C.Point.Longitude ? B.Point.Longitude : C.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(A.Point.Longitude, Math.PI));
                }
                else if ((C.Point.Longitude - A.Point.Longitude) < -Math.PI && (C.Point.Longitude - B.Point.Longitude) < -Math.PI)       //c isolated on LHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, C.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(A.Point.Longitude < B.Point.Longitude ? A.Point.Longitude : B.Point.Longitude, Math.PI));
                }
                else if ((B.Point.Longitude - A.Point.Longitude) < -Math.PI && (B.Point.Longitude - C.Point.Longitude) < -Math.PI)       //b isolated on LHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, B.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(A.Point.Longitude < C.Point.Longitude ? A.Point.Longitude : C.Point.Longitude, Math.PI));
                }
                else if ((A.Point.Longitude - B.Point.Longitude) < -Math.PI && (A.Point.Longitude - C.Point.Longitude) < -Math.PI)       //a isolated on LHS
                {
                    bounds.Add(new Tuple<double, double>(-Math.PI, A.Point.Longitude));
                    bounds.Add(new Tuple<double, double>(B.Point.Longitude < C.Point.Longitude ? B.Point.Longitude : C.Point.Longitude, Math.PI));
                }
                else
                {
                    bounds.Add(new Tuple<double, double>(A.Point.Longitude < B.Point.Longitude ?
                        A.Point.Longitude < C.Point.Longitude ? A.Point.Longitude : C.Point.Longitude :
                        B.Point.Longitude < C.Point.Longitude ? B.Point.Longitude : C.Point.Longitude,
                        A.Point.Longitude > B.Point.Longitude ?
                        A.Point.Longitude > C.Point.Longitude ? A.Point.Longitude : C.Point.Longitude :
                        B.Point.Longitude > C.Point.Longitude ? B.Point.Longitude : C.Point.Longitude));
                }


                return bounds.ToArray();
            }
        }

        public Point3 Normal
        {
            get
            {
                Point3 normal = Point3.Cross(_points[2].Point - _points[1].Point, _points[1].Point - _points[0].Point);
                normal.CartesianToPolar();
                return normal;
            }
        }

        public double PlaneDistance(Point3 ray)
        {
            Point3 normal = Normal;

            double d = Point3.Dot(normal, _points[0].Point);
            double den = Point3.Dot(normal, ray);

            if (den == 0.0) return ray.Length * 100; //whocares


            double t = (Point3.Dot(normal, new Point3(0, 0, 0)) + d) / den;

            if (t < 0.0) return ray.Length * 100; //whocares

            Point3 p = ray * t;
            return p.Length;
        }

        public Barycentric GetBarycentric(Point3 ray)
        {
            Barycentric bary;

            Point3 normal = Normal;

            double d = Point3.Dot(normal, _points[0].Point);
            double den = Point3.Dot(normal, ray);

            if (den == 0.0) return new Barycentric { U = 1, V = 0, W = 0 };

            double t = (Point3.Dot(normal, new Point3(0, 0, 0)) + d) / den;

            if (t < 0.0) return new Barycentric { U = 1, V = 0, W = 0 };

            Point3 p = ray * t;

            double areaABC = normal.Length / 2;

            double areaAPC = Point3.Cross(p - A.Point, C.Point - A.Point).Length / 2;

            double areaAPB = Point3.Cross(p - A.Point, B.Point - A.Point).Length / 2;

            bary.V = areaAPC / areaABC;
            bary.W = areaAPB / areaABC;
            bary.U = 1.0 - (bary.V + bary.W);

            //if (bary.U < 0 || bary.U > 1 ||
            //    bary.V < 0 || bary.V > 1 ||
            //    bary.W < 0 || bary.W > 1) throw new Exception("dangit");

            return bary;
        }

    }

    public struct Barycentric
    {
        public double U, V, W;
    }

    #endregion

    //a single point of the geodesic mesh for the planet.
    //2-way pointers to its neighbours
    class GlobePoint
    {
        #region Static

        private static uint _nextid = 1;
        private static uint NextId { get { return _nextid++; } }

        #endregion

        #region Private Data Members

        private uint _id;
        Point3 _point;
        private List<GlobePoint> _neighbours;
        private List<uint> _neighbourids;
        private Globe _globe;
        private GeoPoint _geo;
        private Plate _plate;

        #endregion

        #region Constructors

        public GlobePoint(Globe g, double x, double y, double z, bool polar = false)
        {
            _globe = g;
            _id = NextId;

            if (polar)
            {
                _point.Latitude = x;
                _point.Longitude = y;
                _point.R = z;
                _point.PolarToCartesian();
            }
            else
            {
                _point.X = x;
                _point.Y = y;
                _point.Z = z;
                _point.CartesianToPolar();
            }

            _neighbours = new List<GlobePoint>();
            _plate = null;
        }

        public GlobePoint(Globe g, BinaryReader br)
        {
            _globe = g;
            _id = br.ReadUInt32();
            _nextid = _nextid <= _id ? _id + 1 : _nextid;

            _point.X = br.ReadDouble();
            _point.Y = br.ReadDouble();
            _point.Z = br.ReadDouble();

            _point.CartesianToPolar();

            _neighbourids = new List<uint>();

            uint n = br.ReadUInt32();
            uint id;

            for (uint i = 0; i < n; i++)
            {
                id = br.ReadUInt32();
                _neighbourids.Add(id);
            }

            _plate = null;
        }

        #endregion

        #region Initializers

        //need this to retrieve neighbour list which has been loaded from file by Globe
        public void GetNeighbours(Globe g)
        {
            if (_neighbourids == null || _neighbours != null) throw new Exception("ya dingus");

            GlobePoint p;

            _neighbours = new List<GlobePoint>();

            foreach (uint id in _neighbourids)
            {
                p = g.GetPoint(id);
                if (p == null) throw new Exception("i know.");
                _neighbours.Add(p);
            }
        }

        public void InitGeo()
        {
            _geo = new GeoPoint(this);
        }

        public void InitTerrain(PlateType type)
        {
            _geo.InitTerrain(type);
        
        }



        #endregion

        #region Accessor Properties

        public uint Id { get { return _id; } }
        public double X { get { return _point.X; } set { _point.X = value; } }
        public double Y { get { return _point.Y; } set { _point.Y = value; } }
        public double Z { get { return _point.Z; } set { _point.Z = value; } }
        public double Latitude { get { return _point.Latitude; } set { _point.Latitude = value; } }
        public double Longitude { get { return _point.Longitude; } set { _point.Longitude = value; } }
        public double R { get { return _point.R; } set { _point.R = value; } }
        public Globe Globe { get { return _globe; } }
        public GeoPoint Geo { get { return _geo; } }
        public Plate Plate { get { return _plate; } set { _plate = value; } }
        public Point3 Point { get { return _point; } }

        #endregion

        #region Neighbour Access Methods

        //these three return 'this' so you can chain them up jQuery style
        public GlobePoint AddNeighbour(GlobePoint neighbour)
        {
            if (HasNeighbour(neighbour)) return this;
            _neighbours.Add(neighbour);
            neighbour.AddNeighbour(this);
            return this;
        }

        public GlobePoint RemoveNeighbour(GlobePoint neighbour)
        {
            if (!HasNeighbour(neighbour)) return this;
            _neighbours.Remove(neighbour);
            neighbour.RemoveNeighbour(this);
            return this;
        }

        public GlobePoint ClearNeighbours()
        {
            foreach (GlobePoint neighbour in _neighbours)
            {
                neighbour.RemoveNeighbour(this);
            }

            _neighbours.Clear();
            return this;
        }

        public uint NeighbourCount { get { return (uint)_neighbours.Count; } }

        public GlobePoint Neighbour(uint i) {return _neighbours[(int)i]; }

        public bool HasNeighbour(GlobePoint gp) { return _neighbours.Contains(gp); }

        public GlobePoint FirstMutualNeighbour(GlobePoint gp2)
        {
            foreach (GlobePoint neighbour in _neighbours)
            {
                if (gp2.HasNeighbour(neighbour)) return neighbour;
            }

            return null;
        }

        #endregion

        #region Build Triangle / Edge Lists

        public List<Triangle> GetTriangles()
        { 
            List<Triangle> triangles = new List<Triangle>();

            for (int i = 0; i < _neighbours.Count; i++)
            {
                for (int j = i + 1; j < _neighbours.Count; j++)
                {
                    if (_neighbours[i].HasNeighbour(_neighbours[j]))
                    {
                        triangles.Add(new Triangle(this, _neighbours[i], _neighbours[j]));
                    }
                }
            }

            return triangles;
        }

        public List<Triangle> GetFilteredTriangles()
        {
            List<Triangle> triangles = new List<Triangle>();

            for (int i = 0; i < _neighbours.Count; i++)
            {
                if (_neighbours[i].Id > _id) continue;
                for (int j = i + 1; j < _neighbours.Count; j++)
                {
                    if (_neighbours[j].Id > _id) continue;
                    if (_neighbours[i].HasNeighbour(_neighbours[j]))
                    {
                        triangles.Add(new Triangle(this, _neighbours[i], _neighbours[j]));
                    }
                }
            }

            return triangles;
        }

        public List<Edge> GetEdges()
        {
            List<Edge> edges = new List<Edge>();

            for (int i = 0; i < _neighbours.Count; i++)
            {
                edges.Add(new Edge(this, _neighbours[i]));
            }

            return edges;
        }

        public List<Edge> GetFilteredEdges()
        {
            List<Edge> edges = new List<Edge>();

            for (int i = 0; i < _neighbours.Count; i++)
            {
                if (_id > _neighbours[i].Id)    edges.Add(new Edge(this, _neighbours[i]));
            }

            return edges;
        }

        #endregion

        #region Polar <=> Cartesian

        public void PolarToCartesian()
        {
            _point.PolarToCartesian();
        }

        public void CartesianToPolar()
        {
            _point.CartesianToPolar();
        }

        #endregion

        #region Serialize

        public void Serialize(BinaryWriter bw)
        {
            bw.Write(_id);
            bw.Write(_point.X);
            bw.Write(_point.Y);
            bw.Write(_point.Z);
            
            bw.Write((uint)_neighbours.Count);

            foreach (GlobePoint neighbour in _neighbours)
            {
                bw.Write(neighbour.Id);
            }
        }

        #endregion

        #region Tick

        public void StartTick(GeoProcess proc)
        {
            _geo.StartTick(proc);
        }

        public void EndTick(GeoProcess proc)
        {
            _geo.EndTick(proc);
        }

        #endregion

    }
}
