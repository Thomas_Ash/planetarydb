﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Tectonic2
{
    #region Enums

    enum Projection { Equi, Lambert, Mercator }

    enum GenerationMethod { Octa, Icosa }

    enum RenderMode { Points, Edges, Triangles, Magma, Plates, PlateType, Terrain, GeoDebug }

    #endregion

    #region Structs

    struct MapPoint
    {
        public MapPoint(double u, double v)
        {
            U = u;
            V = v;
        }

        public MapPoint(double latitude, double longitude, double width, double height, Projection proj)
        {
            if (Math.Abs(latitude) > Math.PI / 2 || Math.Abs(longitude) > Math.PI || width <= 0 || height <= 0)
                throw new Exception("Coordinates out of bounds.");

            U = (longitude / Math.PI + 1) * (width / 2);

            switch (proj)
            {
                case Projection.Equi:
                    V = height * (0.5 - latitude / Math.PI);
                    break;
                case Projection.Lambert:
                    V = height * (0.5 - Math.Sin(latitude) / 2);
                    break;
                case Projection.Mercator:
                    double cutoff = 1.4;
                    if (Math.Abs(latitude) > cutoff) throw new Exception("Coordinates out of bounds.");
                    V = (height / 2) - (width * Math.Log(Math.Tan((Math.PI / 4) + (latitude / 2))) / (2 * Math.PI));
                    break;

                default:
                    throw new Exception("Projection type not supported.");

            }

        }

        public double GetLatitude(double width, double height, Projection proj)
        {
            switch (proj)
            {
                case Projection.Equi:
                    return (1 - (V*2) / height) * Math.PI;
                case Projection.Lambert:
                    return Math.Asin(1 - (V*2) / height);
                case Projection.Mercator:
                    return 2 * Math.Atan(Math.Exp(1 - (V*2) / height)) - Math.PI * 0.5;
                default:
                    throw new Exception("Projection type not supported.");

            }

        }

        public double GetLongitude(double width, double height, Projection proj)
        {
            return (U / width - 0.5) * Math.PI * 2;
        }

        public double U;
        public double V;

        public float UF { get { return (float)U; } }
        public float VF { get { return (float)V; } }

    }

    #endregion

    class Globe
    {

    #region Statics

        public static byte Lerp(byte a, byte b, double x)
        {
            byte lerp = (byte)((double)a * (1 - x) + (double)b * x);
            return lerp;
        }

        public static byte Lerp3(byte a, byte b, byte c, double u, double v, double w)
        {
            byte lerp = (byte)((double)a * u + (double)b * v + (double)c * w);
            return lerp;
        }
        
        public static Color Lerp(Color a, Color b, double x)
        {
            return Color.FromArgb(Lerp(a.A, b.A, x), Lerp(a.R, b.R, x), Lerp(a.G, b.G, x), Lerp(a.B, b.B, x));
        }
        
        public static Color Lerp3(Color a, Color b, Color c, double u, double v, double w)
        {
            return Color.FromArgb(Lerp3(a.A, b.A, c.A, u, v, w),
                Lerp3(a.R, b.R, c.R, u, v, w),
                Lerp3(a.G, b.G, c.G, u, v, w),
                Lerp3(a.B, b.B, c.B, u, v, w));
        }

        public static Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }

        public static Color ColorFromId(uint id)
        { 
            return ColorFromHSV((double)id * 99.78956 % 360, 1, 1);        
        }

    #endregion

        #region Private Data Members

        private List<GlobePoint> _points;
        double _r;  //nominal radius
        double _res; //nominal distance between points
        private List<Edge> _edges;
        private List<Triangle> _triangles;
        private List<Plate> _plates;
        private GlobePointIndex _pointIndex;
        //private TriangleIndex _triangleIndex;
        private TriangleTreeNode _triangleTree;
        private double _normalDrift;

        #endregion

        #region Constructors

        public Globe(GenerationMethod g, double r)
        {
            _points = new List<GlobePoint>();
            _r = r;

            switch (g)
            {
                case GenerationMethod.Octa:

                    _points.Add(new GlobePoint(this, 0, _r, 0));

                    _points.Add(new GlobePoint(this, _r, 0, 0));
                    _points.Add(new GlobePoint(this, 0, 0, _r));
                    _points.Add(new GlobePoint(this, -_r, 0, 0));
                    _points.Add(new GlobePoint(this, 0, 0, -_r));

                    _points.Add(new GlobePoint(this, 0, -_r, 0));

                    _points[0].AddNeighbour(_points[1]).AddNeighbour(_points[2]).AddNeighbour(_points[3]).AddNeighbour(_points[4]);
                    _points[1].AddNeighbour(_points[0]).AddNeighbour(_points[2]).AddNeighbour(_points[4]).AddNeighbour(_points[5]);
                    _points[2].AddNeighbour(_points[0]).AddNeighbour(_points[1]).AddNeighbour(_points[3]).AddNeighbour(_points[5]);
                    _points[3].AddNeighbour(_points[0]).AddNeighbour(_points[2]).AddNeighbour(_points[4]).AddNeighbour(_points[5]);
                    _points[4].AddNeighbour(_points[0]).AddNeighbour(_points[1]).AddNeighbour(_points[3]).AddNeighbour(_points[5]);
                    _points[5].AddNeighbour(_points[1]).AddNeighbour(_points[2]).AddNeighbour(_points[3]).AddNeighbour(_points[4]);

                    _res = _points[0].Point.Distance(_points[1].Point);

                    break;

                case GenerationMethod.Icosa:

                    double lat = Math.Atan(0.5);
                    double lon = 0.2 * Math.PI;

                    _points.Add(new GlobePoint(this, 0.5 * Math.PI, 0, _r, true));

                    _points.Add(new GlobePoint(this, lat, 0, _r, true));
                    _points.Add(new GlobePoint(this, lat, lon * 2, _r, true));
                    _points.Add(new GlobePoint(this, lat, lon * 4, _r, true));
                    _points.Add(new GlobePoint(this, lat, lon * 6, _r, true));
                    _points.Add(new GlobePoint(this, lat, lon * 8, _r, true));

                    _points.Add(new GlobePoint(this, -lat, lon, _r, true));
                    _points.Add(new GlobePoint(this, -lat, lon * 3, _r, true));
                    _points.Add(new GlobePoint(this, -lat, lon * 5, _r, true));
                    _points.Add(new GlobePoint(this, -lat, lon * 7, _r, true));
                    _points.Add(new GlobePoint(this, -lat, lon * 9, _r, true));

                    _points.Add(new GlobePoint(this, -0.5 * Math.PI, 0, _r, true));

                    _points[0].AddNeighbour(_points[1]).AddNeighbour(_points[2]).AddNeighbour(_points[3]).AddNeighbour(_points[4]).AddNeighbour(_points[5]);

                    _points[1].AddNeighbour(_points[2]).AddNeighbour(_points[6]).AddNeighbour(_points[10]);
                    _points[2].AddNeighbour(_points[3]).AddNeighbour(_points[6]).AddNeighbour(_points[7]);
                    _points[3].AddNeighbour(_points[4]).AddNeighbour(_points[7]).AddNeighbour(_points[8]);
                    _points[4].AddNeighbour(_points[5]).AddNeighbour(_points[8]).AddNeighbour(_points[9]);
                    _points[5].AddNeighbour(_points[1]).AddNeighbour(_points[9]).AddNeighbour(_points[10]);


                    _points[6].AddNeighbour(_points[7]);
                    _points[7].AddNeighbour(_points[8]);
                    _points[8].AddNeighbour(_points[9]);
                    _points[9].AddNeighbour(_points[10]);
                    _points[10].AddNeighbour(_points[6]);

                    _points[11].AddNeighbour(_points[6]).AddNeighbour(_points[7]).AddNeighbour(_points[8]).AddNeighbour(_points[9]).AddNeighbour(_points[10]);

                    _res = _points[0].Point.Distance(_points[1].Point);

                    break;
                   
            }

            FindEdgesAndTriangles();
            _pointIndex = new GlobePointIndex(_points);
            _triangleTree = new TriangleTreeNode(_triangles);
            _triangleTree.Build();
            //_triangleIndex = new TriangleIndex(_triangles);

            //FindEdges();
            //FindTriangles();
        }

        public Globe(string filename)
        {
            BinaryReader br = new BinaryReader(File.Open(filename, FileMode.Open));

            _r = br.ReadDouble();

            _points = new List<GlobePoint>(); 

            uint id;
            GlobePoint a,b,c;
            uint n = br.ReadUInt32();

            LogFile.Log.Write(String.Format("Loading {0} points from {1}", n, filename));

            for (uint i = 0; i < n; i++)
            {
                _points.Add(new GlobePoint(this, br));
            }

            _pointIndex = new GlobePointIndex(_points);

            foreach (GlobePoint point in _points)
            {
                point.GetNeighbours(this);
            }

            _edges = new List<Edge>();

            n = br.ReadUInt32();

            LogFile.Log.Write(String.Format("Loading {0} edges from {1}", n, filename));

            _res = 0;
            Edge edge;


           for (uint i = 0; i < n; i++)
            {
               id = br.ReadUInt32();
               a = GetPoint(id);
               id = br.ReadUInt32();
               b = GetPoint(id);
               edge = new Edge(a, b);
               _edges.Add(edge);
               if (_res < edge.Length) _res = edge.Length;
            }

           _triangles = new List<Triangle>();

           n = br.ReadUInt32();

           LogFile.Log.Write(String.Format("Loading {0} triangles from {1}", n, filename));

           for (uint i = 0; i < n; i++)
           {
               id = br.ReadUInt32();
               a = GetPoint(id);
               id = br.ReadUInt32();
               b = GetPoint(id);
               id = br.ReadUInt32();
               c = GetPoint(id);
               _triangles.Add(new Triangle(a, b, c));
           }

           //_triangleIndex = new TriangleIndex(_triangles);
            _triangleTree = new TriangleTreeNode(_triangles);
            _triangleTree.Build();

           LogFile.Log.Write(String.Format("Loaded", n));
           br.Close();
        }

        #endregion

        #region Initializers

        public void Tessellate(uint iterations = 1)
        {
            GlobePoint[] newpoints = new GlobePoint[3];           
            GlobePoint p;


            for (int i = 0; i < iterations; i++)
            {
                LogFile.Log.Write(String.Format("Tessellating {0} points", _points.Count()));

                foreach (Edge edge in _edges)
                {
                    p = new GlobePoint(this, (edge.A.X + edge.B.X) / 2, (edge.A.Y + edge.B.Y) / 2, (edge.A.Z + edge.B.Z) / 2);
                    p.R = _r;
                    p.PolarToCartesian();
                    edge.A.RemoveNeighbour(edge.B);
                    p.AddNeighbour(edge.A).AddNeighbour(edge.B);
                    _points.Add(p);
                }

                foreach (Triangle triangle in _triangles)
                {
                    newpoints[0] = triangle.A.FirstMutualNeighbour(triangle.B);
                    newpoints[1] = triangle.B.FirstMutualNeighbour(triangle.C);
                    newpoints[2] = triangle.C.FirstMutualNeighbour(triangle.A);

                    newpoints[0].AddNeighbour(newpoints[1]);
                    newpoints[1].AddNeighbour(newpoints[2]);
                    newpoints[0].AddNeighbour(newpoints[2]);
                }

                _res /= 2;
                FindEdgesAndTriangles();
                _pointIndex = new GlobePointIndex(_points);
                _triangleTree = new TriangleTreeNode(_triangles);
                _triangleTree.Build();
                //_triangleIndex = new TriangleIndex(_triangles);
                //FindEdges();
                //FindTriangles();
                LogFile.Log.Write(String.Format("Tessellation done", _points.Count()));

            }


        }

        public void InitGeo()
        {
            foreach (GlobePoint gp in _points)
            {
                gp.InitGeo();
            }
        }

        public void InitPlates()
        {
            _plates = new List<Plate>();
            
            foreach(GlobePoint point in _points)
            {
                if (point.Geo.IsMaximum)
                {
                    _plates.Add(new Plate(point));
                }            
            }

            bool found = true;

            int i = 0;
        //    int n = 12;

            while (found)// && ++i < n)
            {
                found = Tick(GeoProcess.ExpandPlates);
                
            }

         //   i = 0;

            foreach(Plate plate in _plates)
            {
                LogFile.Log.Write(String.Format("Plate {0}: {1} points", _plates.IndexOf(plate) + 1, plate.Count));
                i += plate.Count;
            }

            LogFile.Log.Write(String.Format("{0} points of {1} mapped to a plate", i, _points.Count));
        }

        public void ReducePlates(uint max)
        {
            while (_plates.Count > max)
            {
                Tick(GeoProcess.MergePlates);
            }
        }

        public void InitTerrain()
        {
            foreach (Plate plate in _plates)
            {
                plate.InitTerrain();
            }
        
        }

        //normalised to 1 = max wrt resolution
        public void InitDrift(double amount)
        {
            _normalDrift = amount;

            foreach (Plate plate in _plates)
            {
                plate.InitDrift(amount * _res / _r);
            }        
        
        }

        #endregion

        #region Accessors

        public double R { get { return _r; } }
        public double Res { get { return _res; } }
        public double NormalDrift { get { return _normalDrift;  } }

        #endregion

        #region Point Access

        public GlobePoint GetPoint(uint id)
        {

            return _pointIndex.ById(id);
            //foreach (GlobePoint p in _points)
            //{
            //    if (p.Id == id) return p;
            //}

            //return null;
        }

        #endregion

        #region Triangle Access

        public Triangle GetTriangle(double latitude, double longitude)
        {
            List<Triangle> shortlist = _triangleTree.GetTriangles(latitude, longitude);
            List<double> distance = new List<double>();

            Point3 ray = new Point3(latitude: latitude, longitude: longitude, r: _r);
            ray.PolarToCartesian();

            foreach (Triangle t in shortlist)
            {
                distance.Add(t.PlaneDistance(ray));
            }

            int closest = 0;

            for (int i = 1; i < distance.Count; i++)
            {
                if (distance[i] < distance[closest]) closest = i;
            }

            return shortlist[closest];
        }

        public Triangle GetTriangle(double latitude, double longitude, out Barycentric bary)
        {
            List<Triangle> shortlist = _triangleTree.GetTriangles(latitude, longitude);
            List<double> distance = new List<double>();

            Point3 ray = new Point3(latitude: latitude, longitude: longitude, r:_r);
            ray.PolarToCartesian();

            foreach (Triangle t in shortlist)
            {
                distance.Add(t.PlaneDistance(ray));
            }

            int closest = 0;

            for (int i = 1; i < distance.Count; i++)
            {
                if (distance[i] < distance[closest]) closest = i;
            }

            bary = shortlist[closest].GetBarycentric(ray);

     //       LogFile.Log.Write(String.Format("U = {0}, V = {1}, W = {2}", bary.U, bary.V, bary.W)); 

            return shortlist[closest];

        }

        #endregion

        #region Build Edge / Triangle Lists

        public void FindEdgesAndTriangles()
        { 
            _edges = new List<Edge>();
            _triangles = new List<Triangle>();

            foreach (GlobePoint gp in _points)
            {
                foreach (Edge e in gp.GetFilteredEdges())
                {
                    _edges.Add(e);
                }
            }

            foreach (GlobePoint gp in _points)
            {
                foreach (Triangle t in gp.GetFilteredTriangles())
                {
                    _triangles.Add(t);
                }
            }      
        }

        public void FindTriangles()
        { 
            _triangles = new List<Triangle>();

            foreach (GlobePoint gp in _points)
            {
                foreach (Triangle t in gp.GetTriangles())
                {
                    if (!_triangles.Contains(t))
                        _triangles.Add(t);
                }
            }
        }


        public void FindEdges()
        {


            foreach (GlobePoint gp in _points)
            {
                foreach (Edge e in gp.GetEdges())
                {
                    if (!_edges.Contains(e))
                        _edges.Add(e);
                }
            }
        }

        #endregion

        #region Render Map Image Files

        public void Render(string filename, UInt16 width, UInt16 height, Projection proj, RenderMode mode, bool lerp = false)
        {
            if (lerp)
            {

                switch (mode)
                {
                    case RenderMode.Points:
                        RenderPointsLerp(filename, width, height, proj);
                        return;
                    case RenderMode.Triangles:
                        RenderTrianglesLerp(filename, width, height, proj);
                        return;
                    case RenderMode.Plates:
                        RenderPlatesLerp(filename, width, height, proj);
                        return;
                    case RenderMode.GeoDebug:
                        RenderGeoDebugLerp(filename, width, height, proj);
                        return;
                    default:
                        throw new Exception("ya goof!");              
                }

            }

            switch (mode)
            {
                case RenderMode.Edges:
                    RenderEdges(filename, width, height, proj);
                    break;
                case RenderMode.Triangles:
                    RenderTriangles(filename, width, height, proj);
                    break;
                case RenderMode.Magma:
                    RenderMagma(filename, width, height, proj);
                    break;
                case RenderMode.Plates:
                    RenderPlates(filename, width, height, proj);
                    break;
                case RenderMode.PlateType:
                    RenderPlateType(filename, width, height, proj);
                    break;
                case RenderMode.Terrain:
                    RenderTerrain(filename, width, height, proj);
                    break;
                default:
                    throw new Exception("ya goof!");

            
            }
        }

        #region old non lerp



        public void DrawEdge(Edge e, Bitmap bitmap, Graphics g, Pen p, Projection proj)
        {
            MapPoint from = new MapPoint(e.A.Latitude, e.A.Longitude, bitmap.Width, bitmap.Height, proj);
            MapPoint to = new MapPoint(e.B.Latitude, e.B.Longitude, bitmap.Width, bitmap.Height, proj);

            if ((from.U - to.U) > 0.5 * bitmap.Width)
            {
                g.DrawLine(p, from.UF - bitmap.Width, from.VF, to.UF, to.VF);
                g.DrawLine(p, from.UF, from.VF, to.UF + bitmap.Width, to.VF);
            }
            else if ((from.U - to.U) < -0.5 * bitmap.Width)
            {
                g.DrawLine(p, from.UF, from.VF, to.UF - bitmap.Width, to.VF);
                g.DrawLine(p, from.UF + bitmap.Width, from.VF, to.UF, to.VF);
            }
            else
            {
                g.DrawLine(p, from.UF, from.VF, to.UF, to.VF);
            }
        }

        public void FillTriangle(Triangle t, Bitmap bitmap, Graphics g, Color color, Projection proj)
        {
            MapPoint a = new MapPoint(t.A.Latitude, t.A.Longitude, bitmap.Width, bitmap.Height, proj);
            MapPoint b = new MapPoint(t.B.Latitude, t.B.Longitude, bitmap.Width, bitmap.Height, proj);
            MapPoint c = new MapPoint(t.C.Latitude, t.C.Longitude, bitmap.Width, bitmap.Height, proj);

            if ((c.U - a.U) > 0.5 * bitmap.Width && (c.U - b.U) > 0.5 * bitmap.Width)       //c isolated on RHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF - bitmap.Width, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF + bitmap.Width, a.VF), 
                new PointF(b.UF + bitmap.Width, b.VF), 
                new PointF(c.UF, c.VF) });
            }
            else if ((b.U - a.U) > 0.5 * bitmap.Width && (b.U - c.U) > 0.5 * bitmap.Width)       //b isolated on RHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF - bitmap.Width, b.VF), 
                new PointF(c.UF, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF + bitmap.Width, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF + bitmap.Width, c.VF) });
            }
            else if ((a.U - b.U) > 0.5 * bitmap.Width && (a.U - c.U) > 0.5 * bitmap.Width)       //a isolated on RHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF - bitmap.Width, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF + bitmap.Width, b.VF), 
                new PointF(c.UF + bitmap.Width, c.VF) });
            }
            else if ((c.U - a.U) < -0.5 * bitmap.Width && (c.U - b.U) < -0.5 * bitmap.Width)       //c isolated on LHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF + bitmap.Width, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF - bitmap.Width, a.VF), 
                new PointF(b.UF - bitmap.Width, b.VF), 
                new PointF(c.UF, c.VF) });
            }
            else if ((b.U - a.U) < -0.5 * bitmap.Width && (b.U - c.U) < -0.5 * bitmap.Width)       //b isolated on LHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF + bitmap.Width, b.VF), 
                new PointF(c.UF, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF - bitmap.Width, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF - bitmap.Width, c.VF) });
            }
            else if ((a.U - b.U) < -0.5 * bitmap.Width && (a.U - c.U) < -0.5 * bitmap.Width)       //a isolated on LHS
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF + bitmap.Width, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF, c.VF) });

                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF - bitmap.Width, b.VF), 
                new PointF(c.UF - bitmap.Width, c.VF) });
            }
            else
            {
                g.FillPolygon(new SolidBrush(color), new PointF[] { 
                new PointF(a.UF, a.VF), 
                new PointF(b.UF, b.VF), 
                new PointF(c.UF, c.VF) });
            }
        }



        public void RenderEdges(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);
            Pen p = new Pen(Color.White, 1);

            g.Clear(Color.Black);

            foreach (Edge e in _edges)
            {
                DrawEdge(e, bitmap, g, p, proj);
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderTriangles(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);

            g.Clear(Color.Black);


            for (int i = 0; i < _triangles.Count; i++)
            {
                FillTriangle(_triangles[i], bitmap, g, ColorFromId((uint)i), proj);
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderMagma(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);
            double temp;
            Color c;

            g.Clear(Color.Black);

            foreach (Triangle t in _triangles)
            {
                temp = t.A.Geo.Magma + t.B.Geo.Magma + t.C.Geo.Magma;

                if (temp < 0.0)
                    c = Color.Black;
                if (temp < 1.0)
                    c = Color.FromArgb(255, (int)(temp * 256), 0, 0);
                else if (temp < 2.0)
                    c = Color.FromArgb(255, 255, (int)((temp - 1) * 256), 0);
                else if (temp < 3.0)
                    c = Color.FromArgb(255, 255, 255, (int)((temp - 2) * 256));
                else 
                    c = Color.White;

                FillTriangle(t, bitmap, g, c, proj);
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderPlates(string filename, UInt16 width, UInt16 height, Projection proj)
        { 
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);        

            Dictionary<Plate, Color> colourmap = new Dictionary<Plate, Color>();

            for (int i=0;i<_plates.Count; i++)
            {
                colourmap.Add(_plates[i], ColorFromId((uint)i));
            }

            g.Clear(Color.Black);

            foreach (Triangle t in _triangles)
            {

                if(t.A.Plate == null || t.B.Plate == null || t.C.Plate == null)
                    FillTriangle(t, bitmap, g, Color.White, proj);
                else if (t.A.Plate == t.B.Plate && t.B.Plate == t.C.Plate)
                    FillTriangle(t, bitmap, g, colourmap[t.A.Plate], proj);
                else
                    FillTriangle(t, bitmap, g, Color.Black, proj);
            }
        
            bitmap.Save(filename, ImageFormat.Png);        
        }

        public void RenderPlateType(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);

            g.Clear(Color.Black);

            foreach (Triangle t in _triangles)
            {

                if (t.A.Plate == null || t.B.Plate == null || t.C.Plate == null)
                    FillTriangle(t, bitmap, g, Color.White, proj);
                else if (t.A.Plate == t.B.Plate && t.B.Plate == t.C.Plate)
                    FillTriangle(t, bitmap, g, (t.A.Plate.Type == PlateType.Oceanic ? Color.DarkBlue : Color.ForestGreen), proj);
                else
                    FillTriangle(t, bitmap, g, Color.Black, proj);
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderTerrain(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap);

            g.Clear(Color.Black);

            double minRock = _r * 100;
            double maxRock = 0;

            foreach (GlobePoint p in _points)
            {
                minRock = p.Geo.Rock < minRock ? p.Geo.Rock : minRock;
                maxRock = p.Geo.Rock > maxRock ? p.Geo.Rock : maxRock; 
            }

            LogFile.Log.Write(String.Format("min={0:0.000}max={1:0.000}", minRock, maxRock));

            double avgRock;
            
            foreach (Triangle t in _triangles)
            {
                avgRock = (t.A.Geo.Rock + t.B.Geo.Rock + t.C.Geo.Rock) / 3;
                FillTriangle(t, bitmap, g, Lerp(Color.Black, Color.White, (avgRock - minRock) / (maxRock - minRock)), proj);
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        #endregion

        public void RenderPointsLerp(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);

            for (int u = 0; u < width; u++)
            {
                for (int v = 0; v < height; v++)
                {
                    MapPoint mp = new MapPoint(u, v);

                    Barycentric bary;

                    Triangle tri = GetTriangle(mp.GetLatitude(width, height, proj),
                        mp.GetLongitude(width, height, proj), out bary);

                    

                    bitmap.SetPixel(u, v, Lerp3(ColorFromId(tri.A.Id),
                        ColorFromId(tri.B.Id),
                        ColorFromId(tri.C.Id),
                        bary.U, bary.V, bary.W));
                }
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderPlatesLerp(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Dictionary<Plate, Color> colourmap = new Dictionary<Plate, Color>();

            for (int i = 0; i < _plates.Count; i++)
            {
                colourmap.Add(_plates[i], ColorFromId((uint)i));
            }

            Bitmap bitmap = new Bitmap(width, height);

            for (int u = 0; u < width; u++)
            {
                for (int v = 0; v < height; v++)
                {
                    MapPoint mp = new MapPoint(u, v);

                    Barycentric bary;

                    Triangle tri = GetTriangle(mp.GetLatitude(width, height, proj),
                        mp.GetLongitude(width, height, proj), out bary);



                    bitmap.SetPixel(u, v, Lerp3(colourmap[tri.A.Plate],
                        colourmap[tri.B.Plate],
                        colourmap[tri.C.Plate],
                        bary.U, bary.V, bary.W));
                }
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderTrianglesLerp(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Dictionary<Triangle, Color> colourmap = new Dictionary<Triangle, Color>();

            for (int i = 0; i < _triangles.Count; i++)
            {
                colourmap.Add(_triangles[i], ColorFromId((uint)i));
            }

            Bitmap bitmap = new Bitmap(width, height);

            for (int u = 0; u < width; u++)
            {
                for (int v = 0; v < height; v++)
                {
                    MapPoint mp = new MapPoint(u, v);

                    Triangle tri = GetTriangle(mp.GetLatitude(width, height, proj),
                        mp.GetLongitude(width, height, proj));

                    bitmap.SetPixel(u, v, colourmap[tri]);
                }
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        public void RenderGeoDebugLerp(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            Bitmap bitmap = new Bitmap(width, height);

            for (int u = 0; u < width; u++)
            {
                for (int v = 0; v < height; v++)
                {
                    MapPoint mp = new MapPoint(u, v);

                    Barycentric bary;

                    Triangle tri = GetTriangle(mp.GetLatitude(width, height, proj),
                        mp.GetLongitude(width, height, proj), out bary);



                    bitmap.SetPixel(u, v, Lerp3(tri.A.Geo.Debug,
                        tri.B.Geo.Debug,
                        tri.C.Geo.Debug,
                        bary.U, bary.V, bary.W));
                }
            }

            bitmap.Save(filename, ImageFormat.Png);
        }

        #endregion

        #region Write Debug Test Files


        public void DebugTriangles(string filename)
        {
            StreamWriter sw = new StreamWriter(filename);

            for(int i = 0; i< _triangles.Count; i++)
            {
                sw.WriteLine(String.Format("Triangle {0}: points {1}, {2} and {3}", i, _triangles[i].A.Id, _triangles[i].B.Id, _triangles[i].C.Id));
            }

            sw.Close();
        }
        
        public void DebugEdges(string filename)
        {
            StreamWriter sw = new StreamWriter(filename);

            for (int i = 0; i < _edges.Count; i++)
            {
                sw.WriteLine(String.Format("Edge {0}: points {1} and {2}", i, _edges[i].A.Id, _edges[i].B.Id));
            }

            sw.Close();
        }

        public void DebugMapPoints(string filename, UInt16 width, UInt16 height, Projection proj)
        {
            StreamWriter sw = new StreamWriter(filename);

            MapPoint mp;

            foreach (GlobePoint point in _points)
            {
                sw.WriteLine(String.Format("Point {0} = {1:0.000}, {2:0.000}, {3:0.000}", point.Id, point.X, point.Y, point.Z));
                sw.WriteLine(String.Format("    Latitude {1:0.000}, Longitude {2:0.000}", point.Id, point.Latitude, point.Longitude)); 
                

                

                try
                {
                    mp = new MapPoint(point.Latitude, point.Longitude, width, height, proj);                                
                    sw.WriteLine(String.Format("    Mapped to {0:0.000}, {1:0.000}", mp.U, mp.V));
                }
                catch (Exception e)
                {
                    sw.WriteLine(e.Message); 
                }

                point.PolarToCartesian();
                sw.WriteLine(String.Format("    Back to Cartesian: {0:0.000}, {1:0.000}, {2:0.000}", point.X, point.Y, point.Z));


            }


            sw.Close();

        }


        #endregion

        #region Serialize

        public void Serialize(string filename)
        { 
            BinaryWriter bw = new BinaryWriter(File.Open(filename, FileMode.Create));

            bw.Write(_r);
            bw.Write((uint)_points.Count);

            foreach(GlobePoint point in _points)
            {
                point.Serialize(bw);
            }

            bw.Write((uint)_edges.Count);
            foreach (Edge edge in _edges)
            {
                bw.Write(edge.A.Id);
                bw.Write(edge.B.Id);
            }

            bw.Write((uint)_triangles.Count);
            foreach (Triangle triangle in _triangles)
            {
                bw.Write(triangle.A.Id);
                bw.Write(triangle.B.Id);
                bw.Write(triangle.C.Id);
            }




            bw.Close();        
        }

        #endregion

        #region Tick

        public bool Tick(GeoProcess proc)
        {
            bool found = false;

            if ((proc & GeoProcess.ExpandPlates) != 0 && _plates != null)
            {
                foreach (Plate plate in _plates)
                {
                    if (plate.Tick(GeoProcess.ExpandPlates)) found = true;
                }
            }

            if ((proc & GeoProcess.MergePlates) != 0 && _plates != null)
            {
                
                Plate smallest = null;

                foreach (Plate plate in _plates)
                {
                    if (smallest == null)
                    {
                        smallest = plate;
                        continue;
                    }

                    if (smallest.Count > plate.Count)
                        smallest = plate;
                }

                smallest = smallest.Absorb();

                _plates.Remove(smallest);

                foreach (Plate plate in _plates)
                {
                    plate.EraseNeighbour(smallest);
                }

            }


            if ((proc & (GeoProcess.BlurMagma | GeoProcess.BlurTerrain | GeoProcess.Drift)) != 0)
            {
                foreach (GlobePoint gp in _points)
                {
                    gp.StartTick(proc);
                }

                foreach (GlobePoint gp in _points)
                {
                    gp.EndTick(proc);
                }
            }

            return found;
        }

        #endregion

    }
}
