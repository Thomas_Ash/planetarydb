﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tectonic2
{
    #region Enums

    enum PlateType
    { 
        Oceanic,
        Continental
    }

    #endregion

    class Plate
    {
        #region Private Data Members

        private List<GlobePoint> _points;
        private List<GlobePoint> _edgePoints;
        private List<Plate> _neighbours;
        private Random _r;
        private PlateType? _type;
        private double _dx, _dy, _dz;

        #endregion

        #region Constructor

        public Plate(GlobePoint first)
        {
            _points = new List<GlobePoint>();
            _points.Add(first);
            first.Plate = this;
            _edgePoints = new List<GlobePoint>();
            _edgePoints.Add(first);
            _neighbours = new List<Plate>();
            _r = new Random();
            _type = null;
        }

        #endregion

        #region Initialize

        public void InitTerrain()
        {
            _type = (_r.NextDouble() > 0.7) ? PlateType.Continental : PlateType.Oceanic;

            foreach (GlobePoint gp in _points)
            {
                gp.InitTerrain(_type.Value);
            }

        }

        public void InitDrift(double amount)
        {
            _dx = amount * _r.NextDouble() * 2 - amount;
            _dy = amount * _r.NextDouble() * 2 - amount;
            _dz = amount * _r.NextDouble() * 2 - amount;

            foreach (GlobePoint gp in _points)
            {
                gp.Geo.InitDrift(_dx,_dy,_dz);
            }
        
        }

        #endregion

        #region Access

        public bool HasPoint(GlobePoint point)
        {
            return _points.Contains(point);
        }

        public int Count { get { return _points.Count; } }

        public PlateType? Type { get { return _type; } }

        public double DX { get { return _dx; } }
        public double DY { get { return _dy; } }
        public double DZ { get { return _dz; } }

        #endregion

        #region Absorb another plate

        public Plate Absorb()
        {
            Plate smallest = null;

            foreach (Plate neighbour in _neighbours)
            {
                if (smallest == null)
                {
                    smallest = neighbour;
                    continue;
                }

                if (smallest.Count > neighbour.Count)
                    smallest = neighbour;
            }

            if (smallest == this || smallest == null) throw new Exception("what?");

            foreach (GlobePoint gp in smallest._points)
            {
                _points.Add(gp);
                gp.Plate = this;
            }

            foreach (Plate neighbour in smallest._neighbours)
            {
                if (!_neighbours.Contains(neighbour) && neighbour != this)
                {
                    _neighbours.Add(neighbour);
                    neighbour._neighbours.Add(this);
                }
            }

            return smallest;
        }

        public void EraseNeighbour(Plate plate)
        {
            if (_neighbours.Contains(plate)) _neighbours.Remove(plate);
        }

        #endregion

        #region Tick

        public bool Tick(GeoProcess proc)
        {
            if ((proc & GeoProcess.ExpandPlates) != 0) return Expand(0.75);

            return true;
        }

        private bool Expand(double chance)
        {
            bool found = false;
            List<GlobePoint> newPoints = new List<GlobePoint>();
            List<GlobePoint> exEdgePoints = new List<GlobePoint>();
            bool isEdge;
            uint i, n;


            foreach (GlobePoint point in _points)
            {
                n = point.NeighbourCount;

                isEdge = false;

                for (i = 0; i < n; i++)
                {
                    if (point.Neighbour(i).Plate == this) continue;

                    isEdge = true;



                    if (point.Neighbour(i).Plate != null)
                    {
                        // touched another plate
                        if (!_neighbours.Contains(point.Neighbour(i).Plate))
                            _neighbours.Add(point.Neighbour(i).Plate);

                        continue;
                    }
                    if (newPoints.Contains(point.Neighbour(i))) continue;

                    found = true;
                    if(_r.NextDouble() < chance) newPoints.Add(point.Neighbour(i));

                }

                if (!isEdge) exEdgePoints.Add(point);
            }

            foreach (GlobePoint point in newPoints)
            {
                //     if (_points.Contains(point)) throw new Exception("For your points");

                _points.Add(point);
                _edgePoints.Add(point);
                point.Plate = this;
            }

            foreach (GlobePoint point in exEdgePoints)
            {
                _edgePoints.Remove(point);
            }

            return found;        
        
        }



        #endregion

    }
}
