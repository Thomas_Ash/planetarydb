﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tectonic2
{
    class Volcano
    {
        #region Private Data Members

        private Globe _globe;
        private double _latitude, _longitude;
        private double _minSize, _maxSize;
        private int _eruptionsLeft, _period;
        private int _i;

        #endregion

        #region Constructor

        public Volcano(Globe globe, double latitude, double longitude, 
            double minSize, double maxSize, int eruptionsLeft, int period, int delay)
        {
            _globe = globe;
            _latitude = latitude;
            _longitude = longitude;
            _minSize = minSize;
            _maxSize = maxSize;
            _eruptionsLeft = eruptionsLeft;
            _period = period;
            _i = delay;
        }

        #endregion

        #region Tick

        public bool Tick()
        {

            return true;
        }

        #endregion
    }
}
