﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tectonic2
{
    class Program
    {
        static void Main(string[] args)
        {
            //BuildMeshes();
            SandBox();
            //TestIntersect();

        }

        static void SandBox()
        {
            LogFile.Log.Write("Go");

            Globe g = new Globe("globe8.dat");

            g.InitGeo();
            
            for (int i = 0; i < 100; i++)
            {
                g.Tick(GeoProcess.BlurMagma);
            }
            LogFile.Log.Write("Magma done");

        //    g.Render("magma.png", 2000, 700, Projection.Lambert, RenderMode.Magma);
        //    LogFile.Log.Write("Magma rendered");

            g.InitPlates();
            g.ReducePlates(16);
            LogFile.Log.Write("Plates done");

            g.Render("plates.png", 2000, 700, Projection.Lambert, RenderMode.Plates);
            LogFile.Log.Write("Plates rendered");

            g.InitTerrain();

            g.Render("terrain0.png", 2000, 700, Projection.Lambert, RenderMode.Terrain);
            LogFile.Log.Write("Terrain rendered");
            
            g.InitDrift(0.2);

        //    g.Render("debug0.png", 2000, 700, Projection.Lambert, RenderMode.GeoDebug, true);
        //    LogFile.Log.Write("Debug rendered");



            for (int i = 0; i < 10; i++)
            {
                g.Tick(GeoProcess.Drift);

                if (i % 10 != 9 && i > 9) continue;
         //       g.Render(String.Format("debug{0}.png", i+1), 2000, 700, Projection.Lambert, RenderMode.GeoDebug, true);
         //       LogFile.Log.Write("Debug rendered");
                g.Render(String.Format("terrain{0}.png", i + 1), 2000, 700, Projection.Lambert, RenderMode.Terrain);
                LogFile.Log.Write("Terrain rendered");
            }

            //g.Render("triangles.png", 2000, 700, Projection.Lambert, RenderMode.Triangles);
            //LogFile.Log.Write("Triangles rendered");

            //g.Render("triangles_lerp.png", 2000, 700, Projection.Lambert, RenderMode.Triangles, true);
            //LogFile.Log.Write("Triangles rendered with lerp");

            //g.Render("points_lerp.png", 2000, 700, Projection.Lambert, RenderMode.Points, true);
            //LogFile.Log.Write("Points rendered with lerp");

            //g.ReducePlates(16);

            //LogFile.Log.Write("Plates reduced");

            //g.Render("plates.png", 2000, 700, Projection.Lambert, RenderMode.Plates);
            //LogFile.Log.Write("Plates rendered");

            //g.Render("plates_lerp.png", 2000, 700, Projection.Lambert, RenderMode.Plates, true);
            //LogFile.Log.Write("Plates rendered with lerp");

            //g.InitTerrain();

            //LogFile.Log.Write("Terrain done");

            //g.Render("plate_type.png", 2000, 700, Projection.Lambert, RenderMode.PlateType);

            //LogFile.Log.Write("Plate type rendered");


            //for (int i = 0; i < 10; i++)
            //{
            //    g.Tick(GeoProcess.BlurTerrain);
            //}


            //g.Render("terrain.png", 2000, 700, Projection.Lambert, RenderMode.Terrain);

            //LogFile.Log.Write("Terrain rendered");

            LogFile.Log.Close();        
        }

        static void BuildMeshes()
        {
            Globe g = new Globe(GenerationMethod.Icosa, 1000);

            LogFile.Log.Write("Start from icosahedron");

            for (int i = 0; i < 10; i++)
            {
                g.Tessellate();
                LogFile.Log.Write(String.Format("Iteration {0} done", i+1));
                g.Serialize(String.Format("globe{0}.dat", i+1));

            }

            LogFile.Log.Close();
                    
        }
    
    }
}
