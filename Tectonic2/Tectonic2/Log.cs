﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Tectonic2
{
    class LogFile
    {
        private static LogFile _log = null;

        public static LogFile Log
        {
            get 
            {
                if (_log == null) _log = new LogFile();
                return _log;
            }
        }

        private StreamWriter _sw;
        private DateTime _zero;

        LogFile()
        {
            _sw = new StreamWriter("Tectonic2.log");
            _zero = DateTime.Now;
        }

        public void Write(string s)
        {
            string ls = String.Format("{0}: {1}", DateTime.Now.Subtract(_zero).TotalSeconds, s);
            Console.Out.WriteLine(ls);
            _sw.WriteLine(ls);
        }

        public void Close()
        {
            _sw.Close();
        }


    }
}
