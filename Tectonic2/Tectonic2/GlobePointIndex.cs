﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tectonic2
{
    class GlobePointIndex
    {
        #region Private Data Members

        private Dictionary<uint, GlobePoint> _byId;

        #endregion

        #region Constructor

        public GlobePointIndex(List<GlobePoint> points)
        {

            LogFile.Log.Write("Building point index");

            _byId = new Dictionary<uint, GlobePoint>();

            foreach (GlobePoint point in points)
            {
                _byId.Add(point.Id, point);    
            }

            LogFile.Log.Write("Done building point index");
        }

        #endregion

        #region Return Indexed Points

        public GlobePoint ById(uint id)
        {
            return _byId[id];
        }

        #endregion

    }

    class TriangleTreeNode
    {
        private static uint _nextid = 1;
        private static uint NextId { get { return _nextid++; } }
        
        private uint _id; 
        private List<Triangle> _triangles;
        private TriangleTreeNode[] _childNodes;
        double _latMin, _latMax, _lonMin, _lonMax;
        bool _isEndNode;


        //construct root node from all triangles
        public TriangleTreeNode(List<Triangle> triangles)
        {
            _id = NextId;
            _triangles = new List<Triangle>();
            _triangles.AddRange(triangles);

            _childNodes = null;
            _latMin = -Math.PI / 2;
            _latMax = Math.PI / 2;
            _lonMin = -Math.PI;
            _lonMax = Math.PI;

            _isEndNode = false;

//            LogFile.Log.Write(String.Format("Contructed root node {0}", _id));
        }

        //construct child node from bounds, add triangles later
        public TriangleTreeNode(double latMin, double latMax, double lonMin, double lonMax)
        {
            _id = NextId;
            _triangles = new List<Triangle>();
            _childNodes = null;
            _latMin = latMin;
            _latMax = latMax;
            _lonMin = lonMin;
            _lonMax = lonMax;
            _isEndNode = false;

//            LogFile.Log.Write(String.Format("Contructed tree node {0}", _id));
//            LogFile.Log.Write(String.Format("Lat: {0:0.000} - {1:0.000}", _latMin, latMax));
//            LogFile.Log.Write(String.Format("Lon: {0:0.000} - {1:0.000}", _lonMin, lonMax));

        }

        //only adds if lat/lon bounds intersect.
        public bool TestAddTriangle(Triangle triangle)
        {
            if (triangle.MinLat > _latMax) return false;
            if (triangle.MaxLat < _latMin) return false;

            bool matched = false;

            foreach (Tuple<double, double> range in triangle.LongBounds)
            {
                if (range.Item1 > _lonMax) continue;
                if (range.Item2 < _lonMin) continue;
                matched = true;
                break;
            }

            if (!matched) return false;

            _triangles.Add(triangle);
            return true;

        }

        public void Build(int capacity = 12)
        {
            if (_triangles.Count <= capacity)
            {
                _isEndNode = true;
                return;
            }

            _childNodes = new TriangleTreeNode[4];

            _childNodes[0] = new TriangleTreeNode(_latMin, _latMin + (_latMax - _latMin) / 2, _lonMin, _lonMin + (_lonMax - _lonMin) / 2);      //SW
            _childNodes[1] = new TriangleTreeNode(_latMin, _latMin + (_latMax - _latMin) / 2, _lonMin + (_lonMax - _lonMin) / 2, _lonMax);      //SE
            _childNodes[2] = new TriangleTreeNode(_latMin + (_latMax - _latMin) / 2, _latMax, _lonMin, _lonMin + (_lonMax - _lonMin) / 2);      //NW
            _childNodes[3] = new TriangleTreeNode(_latMin + (_latMax - _latMin) / 2, _latMax, _lonMin + (_lonMax - _lonMin) / 2, _lonMax);      //NE

            foreach (Triangle t in _triangles)
            {
                _childNodes[0].TestAddTriangle(t);
                _childNodes[1].TestAddTriangle(t);
                _childNodes[2].TestAddTriangle(t);
                _childNodes[3].TestAddTriangle(t);
            }

            _childNodes[0].Build(capacity);
            _childNodes[1].Build(capacity);
            _childNodes[2].Build(capacity);
            _childNodes[3].Build(capacity);

            _triangles.Clear();
        }

        public List<Triangle> GetTriangles(double lat, double lon)
        {
            if (_isEndNode)
            {
 //               LogFile.Log.Write(String.Format("Returning {0} triangles from node {1}", _triangles.Count, _id));

//                LogFile.Log.Write(String.Format("Target lat/lon: {0:0.000}/{1:0.000}", lat, lon));
//                LogFile.Log.Write(String.Format("Returning node for Lat: {0:0.000} - {1:0.000}, Lon: {2:0.000}, {3:0.000}", _latMin, _latMax, _lonMin, _lonMax));


                return _triangles;
            }

            if (lat > _latMin + (_latMax - _latMin) / 2)
            {
                //north half
                if (lon > _lonMin + (_lonMax - _lonMin) / 2)
                {
                    //NE
                    return _childNodes[3].GetTriangles(lat, lon);
                }
                else
                {
                    //NW
                    return _childNodes[2].GetTriangles(lat, lon);
                }
            }
            else 
            { 
                //south half

                if (lon > _lonMin + (_lonMax - _lonMin) / 2)
                {
                    //SE
                    return _childNodes[1].GetTriangles(lat, lon);

                }
                else
                {
                    //SW
                    return _childNodes[0].GetTriangles(lat, lon);
                }
            }
        }


    }


}
