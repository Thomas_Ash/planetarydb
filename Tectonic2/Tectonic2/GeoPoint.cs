﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Tectonic2
{
    struct TickVar<T>
    {
        public TickVar(T x)
        {
            x1 = x0 = x;
        }

        public T X
        {
            get { return x0; }
            set { x1 = value; }
        }

        public void Tick()
        {
            x0 = x1;
        }

        private T x0, x1;

    }


    [Flags]
    enum GeoProcess
    { 
        BlurMagma = 0x01, 
        ExpandPlates = 0x02,
        MergePlates = 0x04,
        Drift = 0x08,
        BlurTerrain = 0x10
    }

    enum FaultEffect
    { 
        None,               //for DriftFactor as pure rotation around origin
        Subduct,            //applied to lower plate at convergence. spawn volcanoes on other plate seperately
        Collide,            //applied to both plates at convergence when 'lower' plate is still pretty high
        Diverge,            //applied at any divergence
        Transform           //applied at boundary with no significant convergence or divergence
    }

    struct DriftFactor
    {
        public DriftFactor(double weight, GlobePoint source, FaultEffect effect)
        {
            Weight = weight;
            Source = source;
            Effect = effect;
        }

        public double Weight;
        public GlobePoint Source;
        public FaultEffect Effect;
    }

    class GeoPoint
    {
        #region Statics

        private static Random _r = new Random();

        #endregion


        #region Private Data Members

        private GlobePoint _point;
        private TickVar<double> _magma, _rock, _dirt, _water;
        private List<DriftFactor> _drift;
        private TickVar<Color> _debug;

        #endregion

        #region Constructor

        public GeoPoint(GlobePoint point)
        {
            _point = point;
            _magma = new TickVar<double>(_r.NextDouble());

            _debug = new TickVar<Color>(Globe.ColorFromId((uint)Math.Floor((_point.Globe.R * 2 + point.Point.X + point.Point.Y) / _point.Globe.R * 16)));
                
        }

        #endregion

        #region Initializers

        public void InitTerrain(PlateType type)
        {
            _rock = new TickVar<double>(_point.R + (_r.NextDouble() + 1 + _magma.X) * (type == PlateType.Continental ? 1 : -1));

        }

        public void InitDrift(double dx, double dy, double dz)
        {
            Point3 source = _point.Point.RotateX(dx).RotateY(dy).RotateZ(dz);

            source.CartesianToPolar();

            Barycentric bary;

            Triangle tri = _point.Globe.GetTriangle(source.Latitude, source.Longitude, out bary);

            _drift = new List<DriftFactor>();

            _drift.Add(new DriftFactor(bary.U, tri.A, FaultEffect.None));

            if (tri.A.Plate != _point.Plate)
            {
                Point3 driftback = source.RotateX(tri.A.Plate.DX).RotateY(tri.A.Plate.DY).RotateZ(tri.A.Plate.DZ);

                double d = (driftback - _point.Point).Length / (_point.Globe.Res * _point.Globe.NormalDrift); 


                LogFile.Log.Write(String.Format("lat={0:0.00000}lon={1:0.00000}d={2:0.00000}", _point.Latitude, _point.Longitude, d));
            
            }

            _drift.Add(new DriftFactor(bary.V, tri.B, FaultEffect.None));

            if (tri.B.Plate != _point.Plate)
            {
                Point3 driftback = source.RotateX(tri.A.Plate.DX).RotateY(tri.A.Plate.DY).RotateZ(tri.A.Plate.DZ);

                double d = (driftback - _point.Point).Length / (_point.Globe.Res * _point.Globe.NormalDrift); 

                LogFile.Log.Write(String.Format("lat={0:0.00000}lon={1:0.00000}d={2:0.00000}", _point.Latitude, _point.Longitude, d));
            
            }

            _drift.Add(new DriftFactor(bary.W, tri.C, FaultEffect.None));

            if (tri.C.Plate != _point.Plate)
            {
                Point3 driftback = source.RotateX(tri.A.Plate.DX).RotateY(tri.A.Plate.DY).RotateZ(tri.A.Plate.DZ);

                double d = (driftback - _point.Point).Length / (_point.Globe.Res * _point.Globe.NormalDrift); 

                LogFile.Log.Write(String.Format("lat={0:0.00000}lon={1:0.00000}d={2:0.00000}", _point.Latitude, _point.Longitude, d));
            
            }
        }

        #endregion

        #region Accessors

        public double Magma { get { return _magma.X; } }
        public double Rock { get { return _rock.X; } }
        public double Dirt { get { return _dirt.X; } }
        public double Water { get { return _water.X; } }
        public Color Debug { get { return _debug.X; } }

        public bool IsMaximum
        { 
            get
            {
                uint i,n;
                n = _point.NeighbourCount;
                
                for (i = 0; i < n; i++)
                {
                    if (_point.Neighbour(i).Geo.Magma > _magma.X) return false;
                }

                return true;
            }        
        }

        #endregion

        #region Tick

        public void StartTick(GeoProcess proc)
        {
            uint n = _point.NeighbourCount;

            if ((proc & GeoProcess.BlurMagma) != 0)
            {
                double acc = _magma.X;

                for (uint i = 0; i < n; i++)
                {
                    acc += _point.Neighbour(i).Geo.Magma;
                }

                _magma.X = acc / (n + 1);
            }

            if ((proc & GeoProcess.BlurTerrain) != 0)
            {
                double acc = _rock.X;

                for (uint i = 0; i < n; i++)
                {
                    acc += _point.Neighbour(i).Geo.Rock;
                }

                _rock.X = acc / (n + 1);
            }

            if ((proc & GeoProcess.Drift) != 0)
            {
                double acc = 0, a = 0, r = 0, g = 0, b = 0;

                foreach (DriftFactor factor in _drift)
                {
                    if (factor.Effect == FaultEffect.None)
                    {
                        acc += factor.Weight * factor.Source.Geo.Rock;
                    }

                    a += factor.Weight * factor.Source.Geo.Debug.A;
                    r += factor.Weight * factor.Source.Geo.Debug.R;
                    g += factor.Weight * factor.Source.Geo.Debug.G;
                    b += factor.Weight * factor.Source.Geo.Debug.B;

                }

                _rock.X = acc;

                try
                {
                    _debug.X = Color.FromArgb((int)a, (int)r, (int)g, (int)b);
                }
                catch
                {
                    _debug.X = Color.Black;
                }
            }

        }

        public void EndTick(GeoProcess proc)
        {
            if ((proc & GeoProcess.BlurMagma) != 0)
            {
                _magma.Tick();
            }

            if ((proc & GeoProcess.BlurTerrain) != 0)
            {
                _rock.Tick();
            }

            if ((proc & GeoProcess.Drift) != 0)
            {
                _rock.Tick();
                _debug.Tick();
            }
        }

        #endregion

        #region

        #endregion
    }
}
