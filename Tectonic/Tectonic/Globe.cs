﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace Tectonic
{
    enum Projection 
    {
        Equirectangular,
        Lambert,
        Mercator        
    }

    struct Vector<T>
    {
        public Vector(T x, T y) 
        {
            _x = x; _y = y;
        }

        public T x { get { return _x; } set { _x = value; } }
        public T y { get { return _y; } set { _y = value; } }
        public T u { get { return _x; } set { _x = value; } }
        public T v { get { return _y; } set { _y = value; } }
        public T lat { get { return _x; } set { _x = value; } }
        public T lon { get { return _y; } set { _y = value; } }
        public T bearing { get { return _x; } set { _x = value; } }
        public T dist { get { return _y; } set { _y = value; } }

        private T _x;
        private T _y;
    }

    class Globe
    {
        #region Statics

            public static Vector<double> UV(double lat, double lon, int width_U, int height_V, Projection proj)
            {
                if(Math.Abs(lat) > Math.PI / 2 || lon < 0 || lon >= Math.PI * 2 || width_U <= 0 || height_V <= 0)
                    throw new Exception("Coordinates out of bounds.");

                Vector<double> uv = new Vector<double>();

                uv.u = (lon / (Math.PI * 2)) * width_U; 

                switch (proj)
                {
                    case Projection.Equirectangular:
                        uv.v = height_V * (0.5 - lat / Math.PI);
                        break;

                    case Projection.Lambert:
                        uv.v = height_V * (0.5 - Math.Sin(lat) / 2);
                        break;

                    case Projection.Mercator:
                    default:
                        throw new Exception("Projection type not supported.");
            
                }

                return uv;
            }

            public static Vector<double> LatLon(double u, double v, int width_U, int height_V, Projection proj)
            {
                if (u < 0 || v < 0 || u >= width_U || v >= height_V || width_U <= 0 || height_V <= 0) 
                    throw new Exception("Coordinates out of bounds.");

                Vector<double> latlon = new Vector<double>();

                latlon.lon = (u / width_U) * Math.PI * 2; 

                switch (proj)
                {
                    case Projection.Equirectangular:
                        latlon.lat = Math.PI * (0.5 - v / height_V);
                        break;

                    case Projection.Lambert:
                        latlon.lat = Math.Asin(2 * (0.5 - v / height_V));
                        break;

                    case Projection.Mercator:
                    default:
                        throw new Exception("Projection type not supported.");

                }
                return latlon;
            }

            public static string MapRef(Vector<double> radians)
            {
                Vector<double> degrees = new Vector<double>();

                degrees.lat = 360 * (radians.lat / (2 * Math.PI));
                degrees.lon = 360 * (radians.lon / (2 * Math.PI));
                degrees.lon = (degrees.lon + 180) % 360 - 180;

                return String.Format("{0:0.000}°{1}, {2:0.000}°{3}", new object[] {  
                                    Math.Abs(degrees.lat), degrees.lat < 0 ? 'S' : 'N', 
                                    Math.Abs(degrees.lon), degrees.lon < 0 ? 'W' : 'E'});

            }

            public static byte Lerp(byte a, byte b, double x)
            {
            //    if (a == 0 && b == 0) return 0;
                byte lerp = (byte)((double)a * (1 - x) + (double)b * x);
             //   if (lerp == 0) return 0;
                return lerp; 
            }

            public static Color Lerp(Color a, Color b, double x)
            {
                return Color.FromArgb(Lerp(a.A, b.A, x), Lerp(a.R, b.R, x), Lerp(a.G, b.G, x), Lerp(a.B, b.B, x));
            }

            //thanks to http://www.movable-type.co.uk/scripts/latlong.html for these

            public static Vector<double> GreatCircleDest(Vector<double> start, double bearing, double dist, double radius)
            {
                Vector<double> dest = new Vector<double>();

                dest.lat = Math.Asin(Math.Sin(start.lat) * Math.Cos(dist / radius) +
                                    Math.Cos(start.lat) * Math.Sin(dist / radius) * Math.Cos(bearing));
                dest.lon = ((Math.PI * 2) + start.lon + Math.Atan2(Math.Sin(bearing) * Math.Sin(dist / radius) * Math.Cos(start.lat),
                                    Math.Cos(dist / radius) - Math.Sin(start.lat) * Math.Sin(dest.lat))) % (Math.PI * 2);

                return dest;
            }

            public static Vector<double> GreatCircleCourse(Vector<double> start, Vector<double> dest, double radius)
            { 
                Vector<double> course = new Vector<double>();

                course.dist = Math.Acos(Math.Sin(start.lat) * Math.Sin(dest.lat) +
                                        Math.Cos(start.lat) * Math.Cos(dest.lat) * Math.Cos(dest.lon - start.lon)) * radius;


                course.bearing = Math.Atan2(Math.Sin(dest.lon - start.lon) * Math.Cos(dest.lat),
                                            Math.Cos(start.lat) * Math.Sin(dest.lat) - Math.Sin(start.lat) * Math.Cos(dest.lat) * Math.Cos(dest.lon - start.lon));

                return course;
            }

            public static double GreatCircleDistance(Vector<double> start, Vector<double> dest, double radius)
            {

                return Math.Acos(Math.Sin(start.lat) * Math.Sin(dest.lat) + Math.Cos(start.lat) * 
                        Math.Cos(dest.lat) * Math.Cos(dest.lon - start.lon)) * radius;
            }

        #endregion

        #region Data Members

            private Projection _proj;
            private int _width_U, _height_V;    //pixels
            private List<Plate> _plates;
            private double _radius;             //km
            private Bitmap _map;
            private Random _r;

        #endregion

        #region Constructor

            public Globe(Projection proj, int width_U, int height_V, double radius)
            {
                _proj = proj;
                _width_U = width_U;
                _height_V = height_V;
                _plates = new List<Plate>();
                _radius = radius;
                _map = new Bitmap(_width_U, _height_V);
                _r = new Random(1234567);
            }

        #endregion

        #region Initialize

            public void AddPlate(Plate plate)
            {
                _plates.Add(plate);
            }

            public Bitmap GeneratePlates()
            {
                double maprange = 100;

           //     Bitmap b = new Bitmap(_width_U, _height_V);
                Color c;

                for (int u = 0; u < _width_U; u++)
                {
                    for (int v = 0; v < _height_V; v++)            
                    {
                        c = Color.FromArgb(_r.Next(256), 0, 0, 0);
                        _map.SetPixel(u, v, c);
                    }
                }

                NeighbourMap nm = new NeighbourMap(_proj, _width_U, _height_V, _radius, maprange);
                if (!nm.Load())
                {
                    nm.Initialize();
                    nm.Save();
                }

                for (int i = 0; i < 5; i++)
                {
                    _map = Blur(_map, nm);
                }

                _map = DebugAlpha(_map);

                return _map;

            }

            //private Bitmap Crack(Bitmap b, NeighbourMap nm)
            //{
            //    if (b.Width != nm.Width_U || b.Height != nm.Height_V) throw new Exception("who cares?");
                                
            //    Bitmap b2 = new Bitmap(b.Width, b.Height);
            //    Graphics g = Graphics.FromImage(b2);

            //    g.Clear(Color.Black);

            //    byte y;

            //    int u = _r.Next(b.Width);
            //    int v = _r.Next(b.Height);
            //    int u2, v2;

            //    while (b2.GetPixel(u, v).G == b2.GetPixel(u, v).B)
            //    {
            //        b2.SetPixel(u, v, Color.FromArgb(0xff, 0x00, 0xff, 0x00));

            //        y = 0xff;

            //        foreach (Neighbour n in nm.GetNeighbours(u, v))
            //        {
            //            if (y < b.GetPixel(n.p.X, n.p.Y).A)
            //            {
            //                y = b.GetPixel(n.p.X, n.p.Y).A;
            //                u2 = u; v2 = v;
            //            }
            //        }
                
            //        g.DrawLine(new Pen(Color.
            //    }

            //    return b2;
            //}

            private Bitmap Blur(Bitmap b, NeighbourMap nm)
            {
                if (b.Width != nm.Width_U || b.Height != nm.Height_V) throw new Exception("who cares?");
                
                Bitmap b2 = new Bitmap(b.Width, b.Height);
                double acc, acc2, k;
                byte y;

                for (int u = 0; u < b.Width; u++)
                {
                    for (int v = 0; v < b.Height; v++)
                    {
                        acc = 0;
                        acc2 = 0;

                        foreach (Neighbour n in nm.GetNeighbours(u, v))
                        {
                            k = 1.0 - n.d / nm.Range;
                            acc += k * ((double)b.GetPixel(n.p.X, n.p.Y).A / 256);
                            acc2 += k;
                        }

                        y = (byte)(256.0 * Math.Pow(acc / acc2, 1));

                        b2.SetPixel(u, v, Color.FromArgb(y, 0, 0, 0));
                    }
                }

                return b2;
            }

            private Bitmap DebugAlpha(Bitmap b)
            {
                byte y;

                Bitmap b2 = new Bitmap(b.Width, b.Height);

                for (int u = 0; u < b.Width; u++)
                {
                    for (int v = 0; v < b.Height; v++)
                    {
                        y = b.GetPixel(u, v).A;
                        b2.SetPixel(u, v, Color.FromArgb(0xff, y, y, y));
                    }
                }

                return b2;
            }

        #endregion

        #region Render

            public Bitmap Render()
            {
                Bitmap b = new Bitmap(_width_U, _height_V);
                Graphics g = Graphics.FromImage(b);

                //clear
                g.Clear(Color.Black);

                //latitude lines
                double[] latlines = new double[] { 5.0/12 * Math.PI, 4.0/12 * Math.PI, 3.0/12 * Math.PI, 2.0/12 * Math.PI, 1.0/12 * Math.PI,
                            0.0, -1.0/12 * Math.PI, -2.0/12 * Math.PI, -3.0/12 * Math.PI, -4.0/12 * Math.PI, -5.0/12 * Math.PI };

                foreach (double lat in latlines)
                {
                    int v = (int)Math.Floor(UV(lat, 0.0).v);
                    g.DrawLine(new Pen(Color.White, 1), 0, v, _width_U, v);
                }

                //longitude lines

                double[] lonlines = new double[] { 0.0, 1.0/6 * Math.PI, 2.0/6 * Math.PI, 3.0/6 * Math.PI, 4.0/6 * Math.PI, 5.0/6 * Math.PI,
                                    Math.PI, 7.0/6 * Math.PI, 8.0/6 * Math.PI, 9.0/6 * Math.PI, 10.0/6 * Math.PI, 11.0/6 * Math.PI};

                foreach (double lon in lonlines)
                {
                    int u = (int)Math.Floor(UV(0.0, lon).u);
                    g.DrawLine(new Pen(Color.Gray, 1), u, 0, u, _height_V);
                }

                Test(latlines, lonlines);

                //plates

                List<Point> points = new List<Point>();     
                Point point;                                
                Vector<double> centre, corner, course;
                double bearing;

                foreach (Plate plate in _plates)
                {
                    Bitmap b2 = new Bitmap(_width_U, _height_V);    //buffer image for this plate
                    Graphics g2 = Graphics.FromImage(b2);

                    g2.Clear(Color.Transparent);
                    
                    points.Clear();
                    

                    centre = UV(plate.Centre.lat, plate.Centre.lon);

                    int n = 32;                 //# corners of polygon approximating circle

                    Color fill = Color.Black;       //paint alpha only for first pass

                    for (int i = 0; i < n; i++)     //find & UV map each corner
                    { 
                        bearing = (Math.PI * 2 * i) / n;
                        corner = GreatCircleDest(plate.Centre, bearing, plate.Radius, _radius);
                        corner = UV(corner.lat, corner.lon);
                        point = new Point((int)corner.u, (int)corner.v);

                        points.Add(point);
                                            
                    }

                    //plate. may be split into multiple polygons by FixPolygon

                    foreach (List<Point> poly in FixPolygon(points))
                    {
                        //paint alpha to buffer
                        g2.FillPolygon(new SolidBrush(fill), poly.ToArray());

                        //get bounding rectangle
                        Rectangle r = new Rectangle(poly.OrderBy(p => p.X).First().X, poly.OrderBy(p => p.Y).First().Y,
                                                    poly.OrderBy(p => p.X).Last().X, poly.OrderBy(p => p.Y).Last().Y);


                        //iterate through pixels
                        for (int u = r.Left; u <= r.Right && u < _width_U; u++)
                        {
                            for (int v = r.Top; v <= r.Bottom && v < _height_V; v++)
                            {
                                if (b2.GetPixel(u, v).A == 0) continue;     //skip those outside polygon

                                course = GreatCircleCourse(plate.Centre, LatLon(u, v), _radius);      //map back to latitude / longitude then get course from centre

                                b.SetPixel(u, v, plate.GetMapPixel(course.bearing, course.dist));     //get pixel from plate map and copy to main bitmap output

                                
                            }
                        }


                    }                    

                    //centre X
                    //g.DrawLine(new Pen(Color.Lime, 3), (int)centre.u - 5, (int)centre.v - 5, (int)centre.u + 5, (int)centre.v + 5);
                    //g.DrawLine(new Pen(Color.Lime, 3), (int)centre.u - 5, (int)centre.v + 5, (int)centre.u + 5, (int)centre.v - 5);

                
                }

                return b;
            }

            private List<List<Point>> FixPolygon(List<Point> poly)
            {
                List<List<Point>> polys = new List<List<Point>>();
                List<Point> poly1 = new List<Point>();
                List<Point> poly2 = null;
                bool to1 = true;
                double bearing;
                int y;

                poly1.Add(poly[0]);

                for (int i = 1; i < poly.Count; i++)
                {
                    bearing = (Math.PI * 2 * i) / poly.Count;

                    if ((poly[i].X - poly[i - 1].X) > _width_U / 2)
                    {
                        if (Math.Cos(bearing) > 0)
                        {
                            y = (poly[i - 1].Y + poly[i].Y) / 2;
                            
                            //wrap around N pole
                            (to1 ? poly1 : poly2).Add(new Point(0, y));
                            (to1 ? poly1 : poly2).Add(new Point(0, 0));
                            (to1 ? poly1 : poly2).Add(new Point(_width_U, 0));
                            (to1 ? poly1 : poly2).Add(new Point(_width_U, y));

                        }
                        else
                        {
                            //cross terminator westwards 

                            y = (poly[i - 1].Y + poly[i].Y) / 2;

                            if (to1)
                            {
                                if (poly2 != null) throw new Exception("Uh oh!");
                                poly2 = new List<Point>();

                                poly1.Add(new Point(0, y));
                                poly2.Add(new Point(_width_U, y));
                                to1 = false;
                            }
                            else
                            {
                                poly2.Add(new Point(0, y));
                                poly1.Add(new Point(_width_U, y));
                                to1 = true;
                            }
                        }


                   //     fill = Math.Cos(bearing) > 0 ? Color.Red    //wrap around N pole
                     //                               : Color.Yellow;    //cross terminator westwards
                    }
                    else if ((poly[i-1].X - poly[i].X) > _width_U / 2)
                    {

                        if (Math.Cos(bearing) < 0)
                        {
                            y = (poly[i - 1].Y + poly[i].Y) / 2;

                            //wrap around S pole
                            (to1 ? poly1 : poly2).Add(new Point(_width_U, y));
                            (to1 ? poly1 : poly2).Add(new Point(_width_U, _height_V));
                            (to1 ? poly1 : poly2).Add(new Point(0, _height_V));
                            (to1 ? poly1 : poly2).Add(new Point(0, y));
                        }
                        else
                        {
                            //cross terminator eastwards 

                            y = (poly[i - 1].Y + poly[i].Y) / 2;

                            if (to1)
                            {
                                if (poly2 != null) throw new Exception("Uh oh!");
                                poly2 = new List<Point>();

                                poly1.Add(new Point(_width_U, y));
                                poly2.Add(new Point(0, y));
                                to1 = false;
                            }
                            else
                            {
                                poly2.Add(new Point(_width_U, y));
                                poly1.Add(new Point(0, y));
                                to1 = true;
                            }
                        }

                       // fill = Math.Cos(bearing) > 0 ? Color.Blue   
                         //                           : Color.Violet;     //wrap around S pole
                    }

                    (to1 ? poly1 : poly2).Add(poly[i]);
                }

                polys.Add(poly1);
                if (poly2 != null) polys.Add(poly2);
                return polys;
            
            }
                

            private void Test(double[] lats, double[] lons)
            {
                StreamWriter s = new StreamWriter("test.txt");
                Vector<double> point = new Vector<double>();
                Vector<double> uv = new Vector<double>();
                Vector<double> latlon = new Vector<double>();

                foreach (double lat in lats)
                {
                    foreach (double lon in lons)
                    {
                        try
                        {
                            point.lat = lat;
                            point.lon = lon;
                            uv = UV(lat, lon);
                            latlon = LatLon(uv.u, uv.v);

                            s.Write(String.Format("lat={0:0.000},lon={1:0.000} : ", lat, lon));
                            s.Write(String.Format("u={0:0.000},v={1:0.000} : ", uv.u, uv.v));
                            s.Write(String.Format("lat={0:0.000},lon={1:0.000} : ", latlon.lat, latlon.lon));
                            s.WriteLine(MapRef(point));
                        }
                        catch (Exception e)
                        {
                            while (e != null)
                            {
                                s.WriteLine(e.Message);
                                e = e.InnerException;
                            }
                        }
                    }
                }

                s.Close();
            
            }

            public void DumpMaps()
            {
                for (int i = 0; i < _plates.Count; i++)
                {
                    _plates[i].DumpMap(String.Format("map{0}.png", i));                
                }
            
            }

        #endregion

        #region Inner Workings

            //lat & lon in radians
            private Vector<double> UV(double lat, double lon)
            {
                return Globe.UV(lat, lon, _width_U, _height_V, _proj);
            }

            //lat & lon in radians
            private Vector<double> LatLon(double u, double v)
            { 
                return Globe.LatLon(u, v, _width_U, _height_V, _proj);
            }

        #endregion

    }
}
