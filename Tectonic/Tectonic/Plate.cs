﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace Tectonic
{
    class Plate
    {
        #region Data Members

            private Bitmap _map;                //assume square. reuse RGBA channels for whatever
            private Vector<double> _centre;     //lat & lon in radians
            private double _angle;              //compass heading for y- axis of map in radians
            private double _radius;             //km scale for larger dimension of map / 2

        #endregion

        #region Data Access

            public Vector<double> Centre { get { return _centre; } }
            public double Radius { get { return _radius; } }

        #endregion

        #region Map Access

            public Color GetMapPixel(double bearing, double distance)
            {
                if (distance > _radius) return Color.Transparent;

                Vector<double> xy = new Vector<double>();

                xy.x = (double)_map.Width - 1;
                xy.y = (double)_map.Height - 1;

                xy.x = (xy.x + (Math.Sin(bearing - _angle) * distance * xy.x) / _radius) / 2;
                xy.y = (xy.y + (Math.Cos(bearing - _angle) * distance * xy.y) / _radius) / 2;

                Point floor = new Point((int)Math.Floor(xy.x), (int)Math.Floor(xy.y));

                Color left = Globe.Lerp(_map.GetPixel(floor.X, floor.Y), _map.GetPixel(floor.X, floor.Y + 1), xy.y - floor.Y);
                Color right = Globe.Lerp(_map.GetPixel(floor.X + 1, floor.Y), _map.GetPixel(floor.X + 1, floor.Y + 1), xy.y - floor.Y);

                return Globe.Lerp(left, right, xy.x - floor.X);        
            }

        #endregion

        #region Test

            public void DumpMap(string path)
            {
                _map.Save(path, ImageFormat.Png);            
            }

        #endregion

        #region Constructors

            public Plate(double centreLat, double centreLon, double angle, double radius, int xy)
            {
                _centre.lat = centreLat;
                _centre.lon = centreLon;
                _angle = angle;
                _radius = radius;
                _map = new Bitmap(xy, xy);

                Graphics g = Graphics.FromImage(_map);
                List<Point> poly = new List<Point>();

                poly.Add(new Point(xy / 2, 0));
                poly.Add(new Point(xy, xy / 2));
                poly.Add(new Point(xy / 2, xy));
                poly.Add(new Point(0, xy / 2));

                g.DrawPolygon(new Pen(Color.Red), poly.ToArray());
            
            }

        #endregion
    }
}
