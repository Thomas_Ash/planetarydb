﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace Tectonic
{
    class Program
    {
        static void Main(string[] args)
        {
            Globe g = new Globe(Projection.Lambert, 1280, 412, 6000);
            g.AddPlate(new Plate(-1.2, 3.7, 0, 3900, 100));
            g.AddPlate(new Plate(.1, .1, 0, 1000, 10));
            g.AddPlate(new Plate(1.2, 2, 0, 3900, 10));
            g.AddPlate(new Plate(-.5, 6.1, 0, 1500, 100));
            g.DumpMaps();
            Bitmap b = g.GeneratePlates();
            b.Save("plates.png", ImageFormat.Png);
            b = g.Render();
            b.Save("render.png", ImageFormat.Png);
        }
    }
}
