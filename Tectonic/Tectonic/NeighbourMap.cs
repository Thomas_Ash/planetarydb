﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace Tectonic
{

    struct Neighbour
    {
        public Neighbour(int x, int y, double _d)
        {
            p = new Point(x, y);
            d = _d;
        }

        public Point p;
        public double d;
    }

    class NeighbourMap
    {
        #region data members

        private Projection _proj;
        private int _width_U, _height_V;
        private double _radius, _range;
        private List<Neighbour>[] _neighbours;

        #endregion

        #region data access

        public Projection Proj { get { return _proj; } }
        public int Width_U { get { return _width_U; } }
        public int Height_V { get { return _height_V; } }
        public double Radius { get { return _radius; } }
        public double Range { get { return _range; } }

        public List<Neighbour> GetNeighbours(int u, int v)
        {
            return _neighbours[u * _height_V + v];
        }
        #endregion
        #region constructor

        public NeighbourMap(Projection proj, int width_U, int height_V, double radius, double range)
        {
            _proj = proj;
            _width_U = width_U;
            _height_V = height_V;
            _radius = radius;
            _range = range;

            _neighbours = new List<Neighbour>[_width_U * _height_V];
        }

        #endregion

        #region initialize

        public void Initialize()
        {
            Vector<double> start = new Vector<double>();
            Vector<double> dest = new Vector<double>();
            double distance;

            for (int u1 = 0; u1 < _width_U; u1++)
            {
                for (int v1 = 0; v1 < _height_V; v1++)
                {
                    _neighbours[u1 * _height_V + v1] = new List<Neighbour>(); 
                }
            }


            for(int u1 = 0; u1 < _width_U; u1++)
            {
                for(int v1 = 0; v1 < _height_V; v1++)
                {
                    for (int u2 = u1; u2 < _width_U; u2++)
                    {
                        for (int v2 = v1; v2 < _height_V; v2++)
                        {
                            start = Globe.LatLon(u1, v1, _width_U, _height_V, _proj);
                            dest = Globe.LatLon(u2, v2, _width_U, _height_V, _proj);
                            distance = Globe.GreatCircleDistance(start, dest, _radius);

                            if (distance < _range)
                            {
                                _neighbours[u1 * _height_V + v1].Add(new Neighbour(u2, v2, distance));
                                _neighbours[u2 * _height_V + v2].Add(new Neighbour(u1, v1, distance));
                            }
                        }
                    }

                    _neighbours[u1 * _height_V + v1] = _neighbours[u1 * _height_V + v1].OrderBy(x => x.d).ToList();
                }            
            }
        
        
        }

        #endregion

        #region

        public void Save() 
        {
            string filename = String.Format("neighbours_{0}_{1}_{2}.dat", (int)_proj, _width_U, _height_V);
            int n;
            BinaryWriter w = new BinaryWriter(File.Open(filename, FileMode.Create));

            w.Write(_radius);
            w.Write(_range);

            for (int u1 = 0; u1 < _width_U; u1++)
            {
                for (int v1 = 0; v1 < _height_V; v1++)
                {
                    n = _neighbours[u1 * _height_V + v1].Count();
                    w.Write((Int32)n);
                    foreach (Neighbour neighbour in _neighbours[u1 * _height_V + v1])
                    {
                        w.Write((Int32)neighbour.p.X);
                        w.Write((Int32)neighbour.p.Y);
                        w.Write(neighbour.d);
                    }
                }
            }

            w.Close();
        }

        public bool Load()
        {
            string filename = String.Format("neighbours_{0}_{1}_{2}.dat", (int)_proj, _width_U, _height_V);
            int n, x, y;
            double d;
            BinaryReader r;

            try { r = new BinaryReader(File.Open(filename, FileMode.Open)); }
            catch { return false; }

            double radius, range;

            radius = r.ReadDouble();
            range = r.ReadDouble();

            if (radius != _radius || range != _range) 
            {
                r.Close();
                return false; 
            }

            for (int u1 = 0; u1 < _width_U; u1++)
            {
                for (int v1 = 0; v1 < _height_V; v1++)
                {
                    _neighbours[u1 * _height_V + v1] = new List<Neighbour>();

                    n = r.ReadInt32();

                    for (int i = 0; i < n; i++)
                    {
                        x = r.ReadInt32();
                        y = r.ReadInt32();
                        d = r.ReadDouble();

                        _neighbours[u1 * _height_V + v1].Add(new Neighbour(x, y, d));
                    }
                }
            }

            return true;
        }
        
        #endregion



    }
}
